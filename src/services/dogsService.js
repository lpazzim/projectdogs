import axios from '../helpers/AxiosHelper';
import config from '../config';

const baseUrl = config.urlConfig.baseUrl;
class dogsServices {
  static dogInformation(breed, info) {
    if (!info || info === 'null' || info ==='undefined') {
      return axios.get(`${baseUrl}breed/${breed}/images/random`);
    } else {
      return axios.get(`${baseUrl}breed/${breed}/${info}/images/random`);
    }

  }

  static listAll() {
    return axios.get(`${baseUrl}breeds/list/all`);
  }

}

export default dogsServices;