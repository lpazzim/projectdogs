import axios from 'axios';
import { withRouter } from 'react-router-dom';
import config from '../config';

// Triggered somewhere

const axiosInstance = axios.create({
  timeout: config.timeoutApi,
  'Cache-Control': 'no-cache',
});

axiosInstance.interceptors.request.use((configAxios) => {
  const configCustom = configAxios;


  return configCustom;
}, error => Promise.reject(error));

export default withRouter(axiosInstance);


// Access-Control-Allow-Headers: access-control-allow-origin,authorization,content-type,pragma
// Access-Control-Allow-Methods: GET, PUT, POST, DELETE, HEAD, OPTIONS
// Access-Control-Allow-Origin: *
// Date: Thu, 27 Jun 2019 19:26:36 GMT
// Server: Kestrel
// X-Powered-By: ASP.NET

// Access-Control-Allow-Headers: access-control-allow-origin,authorization,content-type,pragma
// Access-Control-Allow-Methods: GET, PUT, POST, DELETE, HEAD, OPTIONS
// Access-Control-Allow-Origin: *
