import React from 'react';
import './App.css';
import DogsContainer from './containers/DogsContainer/DogsContainer';

function App() {
  return (
    <div className="App">
      <DogsContainer/>
    </div>
  );
}

export default App;
