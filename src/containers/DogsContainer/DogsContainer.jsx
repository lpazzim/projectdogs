import React, { Component } from 'react';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import './DogsContainer.scss';
import * as _ from 'lodash';
import dogsServices from '../../services/dogsService';
import { Button } from '@material-ui/core';

class DogsContainer extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dogReturn: null,
      selectedBreed: '',
      listDogs: [],
      classStyle: 'img-loading',
      colors: [
        {
          color: 'blue',
          label: 'Azul',
        },
        {
          color: 'red',
          label: 'Vermelho',
        },
        {
          color: 'white',
          label: 'Branco',
        },
        {
          color: 'black',
          label: 'Preto',
        },
        {
          color: 'green',
          label: 'Verde',
        },
      ],
      fonts: [
        {
          font: 'nunito',
          label: 'Nunito Sans',
        },
        {
          font: 'lexend',
          label: 'Lexend Tera',
        },
        {
          font: 'hind',
          label: 'Hind',
        },
        {
          font: 'roboto',
          label: 'Roboto Condensed',
        },
        {
          font: 'montserrat',
          label: 'Montserrat',
        },
      ],
      fontSelected: '',
      nameSelected: '',
      colorSelected: '',
      seachTerm: '',
      infoSearch: '',
    }

    this.getDog = this.getDog.bind(this);
    this.getListAllDogs = this.getListAllDogs.bind(this);
    this.onChangeBreed = this.onChangeBreed.bind(this);
    this.onChangeColor = this.onChangeColor.bind(this);
    this.onChangeFont = this.onChangeFont.bind(this);
    this.saveInformations = this.saveInformations.bind(this);
  }

  componentDidMount() {
    this.getListAllDogs();

    const font = localStorage.getItem('fontSelected');
    const color = localStorage.getItem('colorSelected');
    const searchTerm = localStorage.getItem('searchTerm');
    const infoSearch = localStorage.getItem('infoSearch');
    const selectedBreed = localStorage.getItem('selectedBreed');

    if (font) {
      this.setState({ fontSelected: font });
    }
    if (color) {
      this.setState({ colorSelected: color });
    }

    if (searchTerm) {
      this.getDog(searchTerm, infoSearch);
    }
    if (selectedBreed) {
      this.setState({ selectedBreed });
    }
  }

  getDog(breed, info) {
    dogsServices.dogInformation(breed, info)
      .then((res) => {
        this.setState({ dogReturn: res.data, classStyle: 'img-loaded' });
      })
  }

  onChangeBreed(event) {
    let info = null;
    if (event) {
      if (event.target.value.info) {
        info = event.target.value.info;
      }
      this.setState({
        selectedBreed: event.target.value.label,
        seachTerm: event.target.value.search,
        infoSearch: event.target.value.info
      }, () => {
        this.getDog(event.target.value.search, info);
      });
    }
  }


  onChangeColor(event) {
    if (event) {
      this.setState({ colorSelected: event.target.value });
    }
  }

  onChangeFont(event) {
    if (event) {
      this.setState({ fontSelected: event.target.value });
    }
  }


  getListAllDogs() {
    dogsServices.listAll()
      .then((res) => {
        const listaDogs = [];
        let itemDog = {
          label: '',
          search: '',
        };

        _.forEach(res.data.message, (el, i) => {
          if (el.length > 0) {
            _.forEach(el, (breed) => {
              itemDog = {
                label: `${breed} ${i}`,
                search: i,
                info: breed,
              }
              listaDogs.push(itemDog);
            });
          } else {
            itemDog = {
              label: i,
              search: i,
            }
            listaDogs.push(itemDog);
          }
        });
        this.setState({ listDogs: listaDogs });
      });
  }

  saveInformations() {
    const { colorSelected, fontSelected, seachTerm, infoSearch, selectedBreed } = this.state;


    localStorage.setItem('fontSelected', fontSelected);
    localStorage.setItem('colorSelected', colorSelected);
    localStorage.setItem('searchTerm', seachTerm);
    localStorage.setItem('infoSearch', infoSearch);
    localStorage.setItem('selectedBreed', selectedBreed);
  }


  render() {
    const { dogReturn,
      classStyle,
      listDogs,
      selectedBreed,
      colors,
      fonts,
      colorSelected,
      fontSelected,
    } = this.state;
    let dogImage;
    let dogBreed;
    const dogItens = [];
    const colorItens = [];
    const fontsItens = [];


    if (selectedBreed) {
      dogBreed = (
        <div className={`name-container ${colorSelected} ${fontSelected}`}>
          <p> {selectedBreed} </p>
        </div>
      );
    }

    if (dogReturn) {
      dogImage = (
        <div className={classStyle}>
          <img alt="dogImage" src={dogReturn.message} key={dogReturn.message} />
        </div>
      );
    }

    if (listDogs.length > 0) {
      _.forEach(listDogs, (el) => {
        dogItens.push(<MenuItem value={el}>{el.label}</MenuItem>);
      });
    }


    _.forEach(colors, (el) => {
      colorItens.push(<MenuItem value={el.color}>{el.label}</MenuItem>);
    });


    _.forEach(fonts, (el) => {
      fontsItens.push(<MenuItem value={el.font}>{el.label}</MenuItem>);
    });


    return (
      <div className="dogs-container">
        <div className="container-filters">
          <div>
            <p className="label-filter">
              Fonte:
          </p>
            <Select
              value={fontSelected}
              onChange={(e) => this.onChangeFont(e)}
              displayEmpty
              name="fonts"
              className="select-filter"
            >
              {fontsItens}
            </Select>
          </div>
          <div>
            <p className="label-filter">
              Cor da fonte:
          </p>
            <Select
              value={colorSelected}
              onChange={(e) => this.onChangeColor(e)}
              displayEmpty
              name="color"
              className="select-filter"
            >
              {colorItens}
            </Select>
          </div>
          <div>
            <p className="label-filter">
              Raça:
          </p>
            <Select
              value={selectedBreed}
              onChange={(e) => this.onChangeBreed(e)}
              displayEmpty
              name="breed"
              key={selectedBreed}
              className="select-filter"
            >
              {dogItens}
            </Select>
          </div>
          <div className="btn-salvar">
            <Button variant="outlined" size="large" color="primary" onClick={() => this.saveInformations()} className="btn-save">
              Salvar
            </Button>
          </div>
        </div>
        <div className="dogs-presentation">
          {dogBreed}
          {dogImage}
        </div>
      </div>

    )
  }
}

export default DogsContainer;